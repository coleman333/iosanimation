import cloneDeep from 'lodash/cloneDeep';
import get from 'lodash/get';
import userAction from '../actions/userAction';
import {createReducer} from 'redux-act';

const initialState = {
    error: {},
    user: {},
    allUsers: [],
    resParseText: {},
    text:"",
    ScanOfDoc:{},
    allItems:[]
};

export default createReducer({
    [userAction.getTestUsers.request]: (state, payload) => {
        const newState = cloneDeep(state);
        return newState;
    },
    [userAction.getTestUsers.ok]: (state, payload) => {
        const newState = cloneDeep(state);
        newState.allUsers = payload.response.data;
        // console.log('this is from react native', newState.allUsers);
        return newState;
    },
    [userAction.getTestUsers.error]: (state, payload) => {
        const newState = cloneDeep(state);
        return newState;
    },

    [userAction.login.request]: (state, payload) => {
        const newState = cloneDeep(state);
        return newState;
    },
    [userAction.login.ok]: (state, payload) => {
        const newState = cloneDeep(state);
        if (payload.response.data.user[0]) {
            newState.user = payload.response.data.user[0];
        }
        else {
            newState.user = {}
        }
        console.log('LOGIN OK', newState.user);
        return newState;
    },
    [userAction.login.error]: (state, payload) => {
        const newState = cloneDeep(state);
        return newState;
    },

    [userAction.readText.request]: (state, payload) => {
        const newState = cloneDeep(state);
        // alert('READ TEXT ', newState.resParseText.text);
        return newState;
    },
    [userAction.readText.ok]: (state, payload) => {
        const newState = cloneDeep(state);
        // const resParseText = get(payload, 'response.data.resParseText');
        const resParseText = get(payload, 'response.data');
        // alert(resParseText.IncomeOrder);
        if (resParseText) {
            // alert(`this is reducer read text: ${resParseText.IncomeOrder}`);
            newState.resParseText = resParseText;
        } else {
            newState.resParseText = '...'
        }
        return newState;
    },
    [userAction.readText.error]: (state, payload) => {
        const newState = cloneDeep(state);
        // alert(`READ TEXT NOT OK ${newState.resParseText.text}`);
        return newState;
    },

    [userAction.saveToBaseEditedResult.request]: (state, payload) => {
        const newState = cloneDeep(state);
        // alert('READ TEXT ', newState.resParseText.text);
        return newState;
    },
    [userAction.saveToBaseEditedResult.ok]: (state, payload) => {
        const newState = cloneDeep(state);
        // const resParseText = get(payload, 'response.data.resParseText');
        const resParseText = get(payload, 'response.data');
        // alert(resParseText.IncomeOrder);
        if (resParseText) {
            // alert(`this is reducer read text: ${resParseText.incomeOrder}`);
            newState.resParseText = resParseText;
        } else {
            newState.resParseText = '...'
        }
        return newState;
    },
    [userAction.saveToBaseEditedResult.error]: (state, payload) => {
        const newState = cloneDeep(state);
        // alert(`READ TEXT NOT OK ${newState.resParseText.text}`);
        return newState;
    },

    [userAction.getCodeResponse.request]: (state, payload) => {
        const newState = cloneDeep(state);
        // alert('READ TEXT ', newState.resParseText.text);
        return newState;
    },
    [userAction.getCodeResponse.ok]: (state, payload) => {
        const newState = cloneDeep(state);
        // const resParseText = get(payload, 'response.data.resParseText');
        const ScanOfDoc = get(payload, 'response.data');
        // alert(ScanOfDoc.serialNumber);
        if (ScanOfDoc) {
            // alert(`this is reducer read text: ${Object.keys(ScanOfDoc)}`);
            // alert(`this is reducer read text: ${ScanOfDoc.serialNumber}`);
            newState.ScanOfDoc = ScanOfDoc;
        } else {
            newState.ScanOfDoc = '...'
        }
        return newState;
    },
    [userAction.getCodeResponse.error]: (state, payload) => {
        const newState = cloneDeep(state);
        // alert(`READ TEXT NOT OK ${newState.ScanOfDoc.serialNumber}`);
        return newState;
    },
    [userAction.getAllItems.request]: (state, payload) => {
        const newState = cloneDeep(state);
        // alert('READ TEXT ', newState.resParseText.text);
        return newState;
    },
    [userAction.getAllItems.ok]: (state, payload) => {
        const newState = cloneDeep(state);
        // const resParseText = get(payload, 'response.data.resParseText');
        const allItems = get(payload, 'response.data');
        // alert(`this is the allItems in reducer${allItems[0].serialNumber}`);
        if (allItems) {
            // alert(`this is reducer read text: ${Object.keys(ScanOfDoc)}`);
            // alert(`this is reducer read text: ${ScanOfDoc.serialNumber}`);
            newState.allItems = allItems;
            // alert(`this is new allItems ${newState.allItems[0].serialNumber}`)
            //        } else {
            // newState.allItems = []
        }
        return newState;
    },
    [userAction.getAllItems.error]: (state, payload) => {
        const newState = cloneDeep(state);
        // alert(`READ TEXT NOT OK ${newState.allItems}`);
        return newState;
    },


}, initialState);
