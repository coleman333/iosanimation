import _ from 'lodash'
import simpleActions from '../actions/actions';
import {createReducer} from 'redux-act';

const initialState = {
    amountOfActiveProcesses: 0,
    theme: 'GRAY',
    lng: 'en',
};

export default createReducer({

    [ simpleActions.addProcess ]: (state, payload) => {
        const newState = _.cloneDeep(state);
        newState.amountOfActiveProcesses++;
        return newState
    },

    [ simpleActions.removeProcess ]: (state, payload) => {
        const newState = _.cloneDeep(state);
        newState.amountOfActiveProcesses--;
        return newState
    },

    [ simpleActions.changeTheme ]: (state, payload) => {
        const newState = _.cloneDeep(state);
        newState.theme = payload;
        return newState;
    },

    [ simpleActions.changeLang ]: (state, payload) => {
        const newState = _.cloneDeep(state);
        newState.lng = payload;
        return newState;
    },

}, initialState);
