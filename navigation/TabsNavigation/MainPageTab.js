import React from 'react';
import { createStackNavigator } from 'react-navigation';

import  MainMenu  from '../../containers/MainMenu/MainMenu';
import  Settings  from '../../containers/Settings/Settings';

// import { Header } from '../header';
import { TabIcon } from '../tab-icon';

export const MainMenuTab = {
    screen: createStackNavigator(
        {
            MainMenu: MainMenu,
            Settings: Settings
        },
        {
            // initialRouteName: MainMenu,
            // navigationOptions: {
                // header: <Header theme="light" align="right" />,
            // },
        },
    ),
    navigationOptions: {
        tabBarLabel: 'Home',
        tabBarIcon: ({ focused } : {focused: boolean}) => <TabIcon tab="home" focused = {focused} />
    },
};
