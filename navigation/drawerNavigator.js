import React from 'react';
import {AsyncStorage, Dimensions} from 'react-native';
import {
    createStackNavigator,
    createDrawerNavigator,
    createMaterialTopTabNavigator
} from 'react-navigation';
import Navigator from "./Navigator";
import { DrawerMenu } from "../containers/DrawerMenu/DrawerMenu";
import { MainMenuTab } from "./TabsNavigation/MainPageTab";
import { BarCodeScannerTab } from "./TabsNavigation/BarCodeScannerTab";
import { TextCameraTab } from "./TabsNavigation/TextCamera";
import { InfoFromBaseByNumberTab } from "./TabsNavigation/InfoFromBaseNumberTab";
import { ItemInfoFromBaseTab } from "./TabsNavigation/ItemInfoFromBaseTab";
import  MainMenu  from "../containers/MainMenu/MainMenu";
import {ThemeContext} from "../Constants/Theme";
import {bindActionCreators} from "redux";
import userAction from "../actions/userAction";
import {changeTheme} from "../actions/actions";
import {connect} from "react-redux";
import {styles} from "../containers/Settings/styles";


const {width} = Dimensions.get('screen');
const DRAWER_WIDTH = width - (width / 5);

// let background='';
// if(AsyncStorage.getItem('THEME')==='DARK'){
//     background = 'black'
// }
// if(AsyncStorage.getItem('THEME')==='GRAY'){
//     background = 'gray'
// }
// // let background = AsyncStorage.getItem('THEME');
//
//
// class ApplicationTabsClass {
//     state ={};
//     static contextType = ThemeContext;
//
//     async componentWillMount() {
//             this.state.styles = styles(this.context);
//     }
//
//     componentWillUpdate(nextProps: Readonly<P>, nextState: Readonly<S>, nextContext: any): void {
//         if(this.context !== nextContext){
//             this.state.styles = styles(nextContext);
//             background = this.state.styles.Background;
//         }
//     }
//
// }
//
// const mapStateToProps = (state) => {
//     return {
//         resParseText: state.userReducer.resParseText
//     }
// };
//
// const mapDispatchToProps = (dispatch) => {
//     return {
//         changeTheme: bindActionCreators(changeTheme, dispatch),
//     }
// };
// export default connect(mapStateToProps, mapDispatchToProps)(ApplicationTabsClass);

export const ApplicationTabs =  createMaterialTopTabNavigator(
    {
        MainMenuTab:             MainMenuTab,
        InfoFromBaseByNumberTab: InfoFromBaseByNumberTab,
        TextScannerTab:          TextCameraTab,
        ItemInfoFromBaseTab:     ItemInfoFromBaseTab,
        BarCodeScannerTab:       BarCodeScannerTab,
    },
    {
        tabBarPosition: 'bottom',
        swipeEnabled: true,
        animationEnabled: true,
        lazy: true,
        // initialRouteName: MainMenuTab,
        tabBarOptions: {
            activeTintColor: 'black',
            inactiveTintColor: 'white',
            showIcon: true,
            showLabel: true,
            upperCaseLabel: false,
            pressColor: '#585858',
            pressOpacity: 0,
            // scrollEnabled
            // tabStyle
            indicatorStyle: {
                backgroundColor: 'red',
                height: 4
            },
            labelStyle: {
                fontSize: 14,
                //color: COLORS.mauve,
                margin: 0,
            },
            iconStyle: {
                margin: 0,
            },
            style: {
                backgroundColor: 'gray',
            },
            allowFontScaling: false,
        },
    },

);

export const DrawerNavigator = createDrawerNavigator({
        ApplicationTabs:ApplicationTabs,
        Navigator: Navigator,
    },
    {
        drawerPosition: 'left',
        drawerWidth: 300,
        contentComponent: ({ navigation }) => <DrawerMenu navigation={navigation}/> ,
    }
);
export const ApplicationNavigator = createStackNavigator({
    drawer: DrawerNavigator,
},{
    navigationOptions: {
        header: null,
    }
})



