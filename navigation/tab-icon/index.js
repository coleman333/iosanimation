import React from 'react';
// import AntDesignIcon from 'react-native-vector-icons/AntDesign';

// import { COLORS } from 'src/constants';
// import { Props } from './types';
import {Image, View} from "react-native";
import {styles} from "./styles";

const IconsActive = {
  home:     require('./icons/active/home.png'),
  wallet:   require('./icons/active/wallet.png'),
  scan:     require('./icons/active/scan.png'),
  rewards:  require('./icons/active/rewards.png'),
  network:  require('./icons/active/profile.png'),
  logout:   require('./icons/active/logout.png')
};

const IconsInActive = {
  home:     require('./icons/inactive/home.png'),
  wallet:   require('./icons/inactive/wallet.png'),
  scan:     require('./icons/inactive/scan.png'),
  rewards:  require('./icons/inactive/rewards.png'),
  network:  require('./icons/inactive/profile.png'),
  logout:   require('./icons/inactive/logout.png')
};

export const TabIcon: React.SFC<Props> = props => {
  return (
    <Image
        source      = {props.focused ? IconsActive[props.tab] : IconsInActive[props.tab]}
        style       = {styles.image }
        resizeMode  = {'contain'}
    />
  )
};
