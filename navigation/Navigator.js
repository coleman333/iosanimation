import React from 'react';
import { createStackNavigator,createDrawerNavigator} from 'react-navigation';

import MainMenu from "../containers/MainMenu/MainMenu";
import textCamera from '../containers/TextCamera/textCamera';
import EditingPage from '../containers/EditingPage/EditingPage';
import Settings from '../containers/Settings/Settings';
import BarCodeScanner from '../containers/BarCodeScanner/BarCodeScanner';
import ItemInfoFromBase from '../containers/ItemInfoFromBase/ItemInfoFromBase';
import EnterTheNumber from '../containers/EnterTheNumber/EnterTheNumber';
import InputCodePage from '../containers/InputCodePage/InputCodePage';
import InfoFromBaseByNumber from '../containers/InfoFromBaseByNumber/InfoFromBaseByNumber';
import InfoFromBaseByNumber2 from '../containers/InfoFromBaseByNumber2/InfoFromBaseByNumber2';
import { Text, View ,TouchableOpacity,Image,  Animated, Easing,LayoutAnimation} from 'react-native';
import settings from '../images/settings2.png';

export const Navigator = createStackNavigator({

    // QrScanner: { screen: QrScanner,
    //     navigationOptions:({ navigation }) =>({
    //         headerLeft:null,
    //         header: null,
    //         headerRight:(
    //             <Button style={{marginRight:10}}
    //                 onPress={() =>navigation.navigate('menuToScanGood')}
    //                 title="Info"
    //                 //color="#fff"
    //             />
    //         )
    //     })
    // },

    // MenuToScanGood: {screen: MenuToScanGood,
    //     navigationOptions:({navigation})=>({
    //         header:null
    //     })
    // },

    textCamera: {screen: textCamera,
        navigationOptions:({navigation})=>({
            header:null
        })
    },
    InputCodePage: {screen: InputCodePage,
        navigationOptions:({navigation})=>({
            header:null
        })
    },
    ItemInfoFromBase: {screen: ItemInfoFromBase,
        navigationOptions:({navigation})=>({
            // header:null
            title: 'все товары',
            headerStyle: {
                backgroundColor: 'gray',

            },
            // headerTintColor: '#fff',
            headerTitleStyle: {
                marginLeft:'25%',
                display:'flex',
                // flex:1,
                // flexDirection:'row',
                // justifyContent:'center',
                alignItems:'center',
                textAlign:'center',
                fontWeight: 'bold',
            },
            headerRight:(
                <TouchableOpacity
                    onPress={()=>{ let {navigate} = navigation;
                        navigate('Settings');}}
                    style={{ justifyContent: 'center' }} >
                    <Image source={settings} style={{width: 40, height: 40}}/>
                </TouchableOpacity>
            ),
            headerRightContainerStyle:{width: 70,justifyContent:'center'}
        })

    },
    EnterTheNumber: {screen: EnterTheNumber,
        navigationOptions:({navigation})=>({
            header:null
        })
    },

    BarCodeScanner: {screen: BarCodeScanner,
        navigationOptions:({navigation})=>({
            header:null
        })
    },
    EditingPage: {screen: EditingPage,
        navigationOptions:({navigation})=>({
            // header:null
            title: 'Подтверждение',
            headerStyle: {
                backgroundColor: 'gray',

            },
            // headerTintColor: '#fff',
            headerTitleStyle: {
                marginLeft:'25%',
                display:'flex',
                // flex:1,
                // flexDirection:'row',
                // justifyContent:'center',
                alignItems:'center',
                textAlign:'center',
                fontWeight: 'bold',
            },
            headerRight:(
                <TouchableOpacity
                    onPress={()=>{ let {navigate} = navigation;
                        navigate('Settings');}}
                    style={{ justifyContent: 'center' }} >
                    <Image source={settings} style={{width: 40, height: 40}}/>
                </TouchableOpacity>
            ),
            headerRightContainerStyle:{width: 70,justifyContent:'center'}
        })
    },
    InfoFromBaseByNumber: {screen: InfoFromBaseByNumber,
        navigationOptions:({navigation})=>({
            // header:null
            title: 'результат запроса',
            headerStyle: {
                backgroundColor: 'gray',

            },
            // headerTintColor: '#fff',
            headerTitleStyle: {
                marginLeft:'15%',
                display:'flex',
                // flex:1,
                // flexDirection:'row',
                // justifyContent:'center',
                alignItems:'center',
                textAlign:'center',
                fontWeight: 'bold',
            },
            headerRight:(
                <TouchableOpacity
                    onPress={()=>{ let {navigate} = navigation;
                        navigate('Settings');}}
                    style={{ justifyContent: 'center' }} >
                    <Image source={settings} style={{width: 40, height: 40}}/>
                </TouchableOpacity>
            ),
            headerRightContainerStyle:{width: 70,justifyContent:'center'}
        })
    },
    InfoFromBaseByNumber2: {screen: InfoFromBaseByNumber2,
        navigationOptions:({navigation})=>({
            // header:null
            title: 'результат запроса',
            headerStyle: {
                backgroundColor: 'gray',

            },
            // headerTintColor: '#fff',
            headerTitleStyle: {
                marginLeft:'15%',
                display:'flex',
                // flex:1,
                // flexDirection:'row',
                // justifyContent:'center',
                alignItems:'center',
                textAlign:'center',
                fontWeight: 'bold',
            },
            headerRight:(
                <TouchableOpacity
                    onPress={()=>{ let {navigate} = navigation;
                        navigate('Settings');}}
                    style={{ justifyContent: 'center' }} >
                    <Image source={settings} style={{width: 40, height: 40}}/>
                </TouchableOpacity>
            ),
            headerRightContainerStyle:{width: 70,justifyContent:'center'}
        })
    },
    Settings: {screen: Settings,
        navigationOptions:({navigation})=>({
            // header:null
            title: 'Настройки',
            headerStyle: {
                backgroundColor: 'gray',

            },
            // headerTintColor: '#fff',
            headerTitleStyle: {
                marginLeft:'30%',
                display:'flex',
                // flex:1,
                // flexDirection:'row',
                // justifyContent:'center',
                alignItems:'center',
                textAlign:'center',
                fontWeight: 'bold',
            },

        })
    },

    MainMenu: {screen: MainMenu,
        navigationOptions:({ navigation }) =>({
        // headerLeft: null,
        //  header: null,
            title: 'Главная',
            headerStyle: {
                backgroundColor: 'gray',

            },
            // headerTransitionPreset:{
            //     headerMode:'fade-in-place'
            //
            // },

            // headerTintColor: '#fff',
            headerTitleStyle: {
                marginLeft:'45%',
                // display:'flex',
                // flex:1,
                // flexDirection:'row',
                // justifyContent:'center',
                // alignItems:'center',
                fontWeight: 'bold',
            },
            headerRight:(
                <TouchableOpacity
                      onPress={()=>{ let {navigate} = navigation;
                          navigate('Settings');}}  >
                    <Image source={settings} style={{width: 40, height: 40}}/>
                </TouchableOpacity>
            ),
            headerRightContainerStyle:{width: 70,justifyContent:'center'}
         })
    },

}, {initialRouteName: 'MainMenu',
    defaultNavigationOptions: {
        gesturesEnabled: false,
    },
    transitionConfig: () => ({
        transitionSpec: {
            duration: 900,
            // easing: Easing.out(Easing.poly(4)),
            easing: Easing.bounce,

            timing: Animated.timing,
        },

        // transitionSpec: {
        //     duration: 700,
        //     create: {
        //         type: LayoutAnimation.Types.spring,
        //         property: LayoutAnimation.Properties.scaleXY,
        //         springDamping: 0.9,
        //     },
        //     update: {
        //         type: LayoutAnimation.Types.spring,
        //         springDamping: 0.9,
        //     },
        // },


        screenInterpolator: sceneProps => {
            const { layout, position, scene } = sceneProps;
            const { index } = scene;

            const height = layout.initHeight;
            // const width = layout.initWidth;

            const translateY = position.interpolate({
                    inputRange: [index - 1, index, index + 1],
                    outputRange: [-height, 0, 0],
            });

            const opacity = position.interpolate({
                inputRange: [index - 1, index - 0.5, index],
                outputRange: [0, 0.5, 1],
            });

            return {opacity, transform: [{ translateY }] };
        },



    })

});

export default Navigator;
