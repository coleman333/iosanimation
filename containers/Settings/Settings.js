import React from 'react';
import {
    StyleSheet,
    Text,
    Button,
    View,
    TextInput,
    TouchableOpacity,
    ScrollView,
    AsyncStorage,
    Switch
} from 'react-native';

import {connect} from "react-redux";
import userAction from '../../actions/userAction';
import {bindActionCreators} from 'redux';
import _ from 'lodash';
import {StackActions, NavigationActions} from 'react-navigation';
import { styles } from './styles'

const IMAGE_HEIGHT = 408;
const IMAGE_HEIGHT_SMALL = 24;
import Cookie from 'react-native-cookie'
import {ThemeContext} from "../../Constants/Theme";
import {changeTheme,changeLang} from "../../actions/actions";
import i18next from 'i18next';

class Settings extends React.Component {

    state = {
        item: 1,
        disAnimate: true,
        menu_expanded: false,
        text: '',
        port: '',
        address: '',
        grayValue: true,
        blackValue: false,
        ruValue: true,
        enValue: false
    };

    static contextType = ThemeContext;

    async componentWillMount() {
        try {
            this.state.styles = styles(this.context);

            const port = await AsyncStorage.getItem('port');
            const address = await AsyncStorage.getItem('address');

            this.setState({port: port, address: address});

            const lang = await AsyncStorage.getItem('lng');
            if(lang === 'ru'){
                this.setState({ruValue: true, enValue: false})
            }
            if(lang === 'en'){
                this.setState({ruValue: false, enValue: true})
            }
        } catch (error) {
            console.log(error);
        }
    }

    componentWillUpdate(nextProps: Readonly<P>, nextState: Readonly<S>, nextContext: any): void {
        if(this.context !== nextContext){
            this.state.styles = styles(nextContext);
        }
    }

    async changeTheme(color){
        switch (color) {
            case 'GRAY' :{
                this.setState({grayValue: true});
                this.setState({blackValue: false});
            }break;
            case 'DARK' :{
                this.setState({grayValue: false});
                this.setState({blackValue: true});
            }break;
        }
        return this.props.changeTheme(color);
    }

    async changeLang(lng){
        switch (lng) {
            case 'ru' :{
                this.setState({ ruValue: true});
                this.setState({ enValue: false});
            }break;
            case 'en' :{
                this.setState({ ruValue: false});
                this.setState({ enValue: true});
            }break;
        }
        await i18next.changeLanguage(lng);
        return this.props.changeLang(lng);
    }

    async confirmText() {
        try {
            const port = _.defaultTo(this.state.port, 3003);
            const address = _.defaultTo(this.state.address, '192.168.0.201');

            if (!_.isNaN(+port)) {
                await AsyncStorage.setItem('port', port);
            }

            if (!_.isEmpty(_.trim(address))) {
                await AsyncStorage.setItem('address', address);
            }

            this.props.navigation.navigate('MainMenu');
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        const { styles } = this.state;
        const { t } = this.props;

        return (
            <View style={styles.MainContainer}>
                <View style={ styles.InnerMainContainer }>
                    <ScrollView style={styles.scrollViewContainer} >
                        <View >
                            <View style={styles.ContainerLabel}>
                                <Text style={styles.textLabel}>{ i18next.t('Settings:portNumber') }</Text>
                            </View>

                            <TextInput
                                multiline={true}
                                numberOfLines={4}
                                blurOnSubmit={false}
                                onChangeText={(port) => this.setState({port})}
                                value={this.state.port}
                                style={styles.textInput}
                            />
                        </View>
                        <View >
                            <View style={styles.ContainerLabel}>
                                <Text style={styles.textLabel}>{ i18next.t('Settings:addressConnection') }</Text>
                            </View>
                            <TextInput
                                multiline={true}
                                numberOfLines={4}
                                blurOnSubmit={false}
                                onChangeText={(address) => this.setState({address})}
                                value={this.state.address}
                                style={styles.textInput}
                            />
                        </View>
                        <View style={{width:'100%'}}>
                            <View style={styles.switchContainer}>
                                <Text style={styles.switchTextLabel} >{ i18next.t('Settings:themeDark') }</Text>
                                <View style={styles.innerSwitchViewContainer }>
                                    <Switch
                                        onValueChange = {this.changeTheme.bind(this,'DARK')}
                                        value = {this.state.blackValue}
                                    />
                                </View>
                            </View>
                            <View style={styles.switchContainer}>
                                <Text style={styles.switchTextLabel} >{ i18next.t('Settings:themeGray') }</Text>
                                <View style={styles.innerSwitchViewContainer}>
                                    <Switch
                                         onValueChange = {this.changeTheme.bind(this,'GRAY')}
                                         value = {this.state.grayValue}
                                    />
                                </View>
                            </View>
                        </View>

                        <View style={{width:'100%'}}>
                            <View style={styles.switchContainer}>
                                <Text style={styles.switchTextLabel} >{ i18next.t('Settings:themeEn') }</Text>
                                <View style={styles.innerSwitchViewContainer }>
                                    <Switch
                                        onValueChange = {this.changeLang.bind(this,'en')}
                                        value = {this.state.enValue}

                                    />
                                </View>
                            </View>
                            <View style={styles.switchContainer}>
                                <Text style={styles.switchTextLabel} >{ i18next.t('Settings:themeRu') }</Text>
                                <View style={styles.innerSwitchViewContainer}>
                                    <Switch
                                         onValueChange = {this.changeLang.bind(this,'ru')}
                                         value = {this.state.ruValue}
                                    />
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </View>
                <View style={{flex: 1}}>
                    <TouchableOpacity onPress={this.confirmText.bind(this)} style={styles.buttonStyle}>
                        <Text style = { styles.switchTextLabel } >{ i18next.t('Settings:confirmButton') }</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        resParseText: state.userReducer.resParseText
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        userAction: bindActionCreators(userAction, dispatch),
        changeTheme: bindActionCreators(changeTheme, dispatch),
        changeLang: bindActionCreators(changeLang, dispatch),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
