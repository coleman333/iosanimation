'use strict';
import React, {Component} from 'react';
import {
    ActivityIndicator,
    AppRegistry,
    Dimensions, Image,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import userAction from "../../actions/userAction";
import {connect} from "react-redux";
import {bindActionCreators} from 'redux';
import EditingPage from '../EditingPage/EditingPage';
import BordersForCamera from '../../components/BordersForCamera';
import settings from "../../images/settings2.png";
import smartItLogo from "../../images/SmartITLogo.png";
import {styles2} from './styles';
class BarCodeScanner extends Component {

    componentDidMount(){
        // if(this.props.text){
        //     let { navigate } = this.props.navigation;
        //     navigate('MainMenu');
        // }
    }

    render() {
        const {amountOfActiveProcesses: processing} = this.props;
// alert(`amount of processes: ${processing}`)
        return (
            <View style={styles.container}>
                {/*{!processing && <View style={{flex: 0, flexDirection: 'row', justifyContent: 'center',}}>*/}
                    {/*<TouchableOpacity*/}
                    {/*onPress={this.takePicture.bind(this)}*/}
                    {/*style={styles.capture}*/}
                    {/*>*/}
                    {/*<Text style={{fontSize: 14}}> SNAP </Text>*/}
                    {/*</TouchableOpacity>*/}

                {/*</View> }*/}
                {!processing && <View style={{flex:1}}>

                    {/*<View style={{flex:6}}>*/}
                        <RNCamera
                            ref={ref => {
                                this.camera = ref;
                            }}
                            captureAudio={false}
                            style={styles.preview}
                            type={RNCamera.Constants.Type.back}
                            flashMode={RNCamera.Constants.FlashMode.off}
                            permissionDialogTitle={'Permission to use camera'}
                            permissionDialogMessage={'We need your permission to use your camera phone'}
                            onGoogleVisionBarcodesDetected={({barcodes}) => {
                                const barcodeResult = barcodes[0].data;
                                this.setState({barcodeResult:barcodeResult});
                                this.props.userAction.getCodeResponse(barcodeResult)
                                    .then(()=>{
                                        // this.props.navigation.navigation('InfoFromBaseByNumber')
                                        let { navigate } = this.props.navigation;
                                        navigate('InfoFromBaseByNumber');
                                    })
                                    .catch(err=>alert(`this is the exception in bar code scanner ==> ${err} ==>${barcodeResult}`));

                                // alert(barcodeResult);
                            }}
                            // onFacesDetected={({face}) => {
                            //     alert(face)
                            // }}
                        >
                            <View style={{flex:1}}>
                            </View>
                            <View style={{flex:2, flexDirection: 'column'}}>
                                <Text style={{color:'white',fontSize: 20, marginLeft:'10%', flexDirection:'row'}}>Для входа в систему, </Text>
                                <Text style={{color:'white',fontSize: 20, flexDirection:'row'}}>просканируйте личный QR </Text>
                                <Text style={{color:'white',fontSize: 20, marginLeft:'30%', flexDirection:'row'}}>код</Text>
                            </View>
                            <View style={{flex:8}}>
                            </View>
                            <BordersForCamera/>
                        </RNCamera>
                    {/*</View>*/}
                </View>
                }

                {!!processing && <View style={[styles2.container]}>
                        <View style={{flex:1}}>
                            <View style={{flex:1}}>
                            </View>
                            <View style={{flex:1}}>
                                <Image source={smartItLogo} style={{width: 40, height: 40}}/>
                            </View>
                            <View style={{flex:1}}>
                                <View style={{flex:1}}>
                                    <ActivityIndicator size="large" color="#0000ff"/>
                                </View>
                                <View style={{flex:1}}>
                                    <Image source={settings} style={{width: 40, height: 40}}/>
                                </View>
                            </View>
                        </View>
                        <Text>Идет обработка </Text>


                    </View>
                }
            </View>
        );
    }

    takePicture = async function () {
        if (this.camera) {
            const options = {quality: 1.0, base64: true, doNotSave: false};
            const data = await this.camera.takePictureAsync(options)
            // alert(data.base64);
            this.props.userAction.readText(data)
                .then(()=>{
                    // if(this.props.userReducer.text){
                    let { navigate } = this.props.navigation;
                    navigate('EditingPage');
                    // }
                })
                .catch(err=>alert(`Some error was thrown to the component. ${err.error.message} - ${err.stack}`))
        }
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        // backgroundColor: 'transparent',
        // backgroundColor: 'black'
    },
    preview: {
        flex: 1,
        // justifyContent: 'flex-end',
        alignItems: 'center'
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20
    }
});

const mapStateToProps = (state) => {
    return {
        amountOfActiveProcesses: state.common.amountOfActiveProcesses,
        text: state.userReducer.readText
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        userAction: bindActionCreators(userAction, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(BarCodeScanner);