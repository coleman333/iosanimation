import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    ScrollView,
    Animated,
    Keyboard,
    Easing, AsyncStorage,
} from 'react-native';

import {connect} from "react-redux";
import userAction from '../../actions/userAction';
import {bindActionCreators} from 'redux';
// import {FormLabel, FormInput, FormValidationMessage} from 'react-native-elements'
import _ from 'lodash';
import BadInstagramCloneApp from '../TextCamera/textCamera';
import { styles } from './styles';
import changeLang from '../../actions/actions';

// import {Input} from 'react-native-elements';


import { StackActions, NavigationActions} from 'react-navigation';

const IMAGE_HEIGHT = 408;
const IMAGE_HEIGHT_SMALL = 24;
import Cookie from 'react-native-cookie'
import { ThemeContext } from "../../Constants/Theme";


class InfoFromBaseByNumber extends React.Component {
    spinValue = new Animated.Value(0);
    _animValue2 = new Animated.Value(0);
    state = {
        toScroll: false,
        keyboardHeight: 0,
        normalHeight: 0,
        shortHeight: 0,
        heightOfElementWhenKeyboardOn: '100%',
        item: 1,
        // launchAnimation: 'active',
        facebookName: '',
        isntaToken: '',
        disAnimate: true,
        menu_expanded: false,
        text: '',
        isKeyboardActive:false,
    };

    constructor(props) {
        super(props);
        this.myRef = React.createRef();
        this.y_translate = new Animated.Value(0);

    }
    // spring = new Animated.Value(0);
    static contextType = ThemeContext;

    async componentWillMount() {
        this.state.styles = styles(this.context);

        this.animatedValue1 = new Animated.Value(1);

        if (this.props.navigation.state.params) {
            const {page} = this.props.navigation.state.params;
            if (page === "unAuthorized") {
                alert('Вы не зарегистрированы')
            }
        }
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));

        this.setState({launchAnimation: 'inActive'})

        // this.onChange=_.debounce(this.onChange.bind(this), 1000);
    }

    componentWillUpdate(nextProps: Readonly<P>, nextState: Readonly<S>, nextContext: any): void {
        if(this.context !== nextContext){
            this.state.styles = styles(nextContext);
        }
    }

    componentDidMount() {

        Animated.loop(
            Animated.timing(
                this.spinValue,
                {
                    toValue: 1,
                    duration: 3000,
                    // easing: Easing.linear,
                    useNativeDriver: true
                }
            )
        ).start();
        this.setState({
            serialNumber: this.props.ScanOfDoc.serialNumber,
            incomeOrder: this.props.ScanOfDoc.incomeOrder,
            providerCode: this.props.ScanOfDoc.providerCode,
            guestNumber: this.props.ScanOfDoc.guestNumber,
            buyingOrder: this.props.ScanOfDoc.buyingOrder
        });
        // alert('this is from editing page',this.state.text);
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow(e) {
        this.setState({isKeyboardActive:true})
    }

    _keyboardDidHide() {
        this.setState({isKeyboardActive:false})
    }

    netsAuthorization() {
        Animated.timing(
            this._animValue2,
            {
                toValue: 1,
                duration: 2000,
                easing: Easing.bounce,
                useNativeDriver: true
            }
        ).start();
        this.setState({disAnimate: false})
    }

    authPanelDisAnimate() {
        console.log('Test press');

        Animated.timing(
            this._animValue2,
            {
                toValue: 0,
                duration: 2000,
                easing: Easing.bounce,
                useNativeDriver: true
            }
        ).start();
        this.setState({disAnimate: true})
    }

    handlePressIn() {
        Animated.spring(this.animatedValue1, {
            toValue: 0.5
        }).start()
    }

    handlePressOut() {
        Animated.spring(this.animatedValue1, {
            duration: 2000,
            toValue: 1,
            friction: 4,
            tension: 40
        }).start()
    }

    readText() {
        // this.props.userAction.readText() //because of it has no arguments passed through.
    }

    redirectToCamera() {
        let {navigate} = this.props.navigation;
        navigate('BadInstagramCloneApp');
    }

    confirmText(){
        const confirmedText ={
            incomeOrder:this.state.incomeOrder,
            providerCode:this.state.providerCode,
            guestNumber:this.state.guestNumber,
            buyingOrder:this.state.buyingOrder,
        };
        this.props.userAction.saveToBaseEditedResult(confirmedText)
            .then(()=>{
                this.props.navigation.navigate('ItemInfoFromBase');
            })

    }

    getItemInfo(){


    }
    render() {
        const {toScroll, styles} = this.state;
        const inputAccessoryViewID = "uniqueID";

        const spin = this.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        })

        const slide2 = this._animValue2.interpolate({
            inputRange: [0, 1],
            outputRange: [-500, 0],

        })
        const animatedStyle1 = {transform: [{scale: this.animatedValue1}]}

        return (
            <View style={ styles.MainContainer }>
                <View style={ styles.MainInnerContainer }>
                    <ScrollView style={{width:"90%"}}>
                        <View style={styles.ContainerLabel}>
                            <Text style={styles.TextLabel}>Номер товара</Text>
                        </View>

                        <TextInput
                            onChangeText={(serialNumber) => this.setState({serialNumber:serialNumber})}
                            value={this.state.serialNumber}
                            style={styles.textInput}
                        />
                        <View style={styles.ContainerLabel}>
                            <Text style={styles.TextLabel}>Номер Документа</Text>
                        </View>

                        <TextInput
                            onChangeText={(incomeOrder) => this.setState({incomeOrder:incomeOrder})}
                            value={this.state.incomeOrder}
                            style={styles.textInput}
                        />
                        <View style={styles.ContainerLabel}>
                            <Text style={styles.TextLabel}>Код Поставщика</Text>
                        </View>
                        <TextInput
                            onChangeText={(providerCode) => this.setState({providerCode})}
                            value={this.state.providerCode}
                            style={styles.textInput}
                        />
                        <View style={styles.ContainerLabel}>
                            <Text style={styles.TextLabel}>Номер сопроводительного документа</Text>
                        </View>
                        <TextInput
                            onChangeText={(guestNumber) => this.setState({guestNumber})}
                            value={this.state.guestNumber}
                            style={styles.textInput}
                        />
                        <View style={styles.ContainerLabel}>
                            <Text style={styles.TextLabel}>Заказ на покупку</Text>
                        </View>
                        <TextInput
                            onChangeText={(buyingOrder) => this.setState({buyingOrder})}
                            value={this.state.buyingOrder}
                            style={styles.textInput}
                        />
                    </ScrollView>
                    {/*</TouchableWithoutFeedback>*/}
                </View>
            </View>


        );
    }
}
//
// const styles =StyleSheet.create({
//     ContainerLabel:{
//         marginTop:10,
//         // height: 50,
//         width:'100%',
//         // backgroundColor: '#eee',
//         // borderWidth:1,
//         // borderRadius:25,
//         justifyContent: 'center',
//     },
//     textLabel:{
//         textAlign:'center',
//         alignItems:'center',
//     },
//     textInput:{
//         borderColor:'black',
//         marginTop:10,
//         height: 50,
//         width:'100%',
//         backgroundColor: '#eee',
//         borderWidth:1,
//         borderRadius:25,
//         justifyContent: 'center',
//         textAlign: "center"
//     }
// });

const mapStateToProps = (state) => {
    console.log(state.dimension);
    return {
        resParseText: state.userReducer.resParseText,
        ScanOfDoc:state.userReducer.ScanOfDoc
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        userAction: bindActionCreators(userAction, dispatch),
        changeLang: bindActionCreators(changeLang, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(InfoFromBaseByNumber);
