import React from "react";
import {SafeAreaView, View, TouchableHighlight, Image, Text, StyleSheet, TouchableOpacity,AsyncStorage} from 'react-native';
import  InputCodePage  from '../InputCodePage/InputCodePage';
import  Settings  from '../Settings/Settings';
// import Navigation from 'react-navigation';
import settings from '../../images/settings2.png';
import {ThemeContext} from '../../Constants/Theme'
import {liStyle} from './styles';

export class DrawerMenu extends React.Component<Props, State> {

    state={};

    componentWillMount() {
        this.state.styles = liStyle(this.context);
    }
    // Specify our dependencies.
    static contextType = ThemeContext;

      pressed_Network = (): void => {
        this.props.navigation.navigate('InputCodePage');
    };

     pressed_Scan = (): void => {
        this.props.navigation.navigate('Settings');
    };
    // async componentDidMount(): void {
    //     const THEME = await AsyncStorage.getItem('THEME');
    //     this.setState({theme:THEME});
    //     console.log(THEME);
    // }


    componentWillUpdate(nextProps, nextState, nextContext) {
        if (this.context !== nextContext) {
            this.state.styles = liStyle(nextContext);
        }
    }

    render(): React.ReactNode {
        const {styles} = this.state;
        // console.log('theme',this.context)
         // const { gray, dark,theme } = this.props;
         return (
            <SafeAreaView style={[styles.mainContainer ]}>
                <View style = {styles.firstSeparator}></View>
                <TouchableOpacity style={styles.menuItem} title = "Profile" onPress={this.pressed_Network}>
                    <View style={styles.imageContainer}>
                        <Image source={settings} style={styles.iconStyle}/>
                    </View>
                    <View  style={styles.textContainer} >
                        <Text style={styles.textStyle}>Input</Text>
                    </View>
                </TouchableOpacity>

                <View style = {styles.separator}></View>

                <TouchableOpacity style={styles.menuItem} title = "Scan"  onPress={this.pressed_Scan}>
                    <View style={styles.imageContainer}>
                        <Image source={settings} style={styles.iconStyle}/>
                    </View>
                    <View  style={styles.textContainer} >
                        <Text style={styles.textStyle}>Setting</Text>
                    </View>
                </TouchableOpacity>
                <View style = {styles.separator}></View>


            </SafeAreaView>
        );
    }

}
