import React from 'react';
import {
    Text,
    View,
    Animated,
    Keyboard,
    Dimensions,
    Image,
    Easing,
    TouchableWithoutFeedback,
    AsyncStorage
} from 'react-native';

import {connect} from "react-redux";
import userAction from '../../actions/userAction';
import {bindActionCreators} from 'redux';
import {changeTheme} from '../../actions/actions';
import { animButtonStyle, st, Container, upContainer } from './style.js'
import SlidingUpPanel from 'rn-sliding-up-panel';
// import {FormLabel, FormInput, FormValidationMessage} from 'react-native-elements'
import _ from 'lodash';
import BadInstagramCloneApp from '../TextCamera/textCamera';
import InputCodePage from '../InputCodePage/InputCodePage';

import {StackActions, NavigationActions} from 'react-navigation';
// import { withTranslation, Trans ,WithNamespaces } from 'react-i18next';
import i18next from  'i18next';
// import { TranslationFunction } from "i18next";


const IMAGE_HEIGHT = 408;
const IMAGE_HEIGHT_SMALL = 24;
import Cookie from 'react-native-cookie'
import {ThemeContext} from "../../Constants/Theme";


class MainMenu extends React.Component {
    constructor(props) {

        super(props);
        this.myRef = React.createRef();
        this.y_translate = new Animated.Value(0);
    }

    authorization() {
        // this.props.userAction.getTestUsers();
        // const { navigate } = this.props.navigation;

        // console.log(navigate)
        // navigate('menuToScanGood');


        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({routeName: 'BadInstagramCloneApp'}, {page: 'auth'}),
            ],
        });
        this.props.navigation.dispatch(resetAction);


        // navigate('QrScanner');
        // this.props.userAction.login(name);
    }

    static contextType = ThemeContext;

    componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any): void {
    }

    componentWillMount() {
        this.state.animButtonStyle = animButtonStyle(this.context);
        this.state.st = st(this.context);
        this.state.Container = Container(this.context);


        this.animatedValue1 = new Animated.Value(1);
        this.animatedValue11 = new Animated.Value(1);
        this.animatedValue2 = new Animated.Value(1);
        this.animatedValue3 = new Animated.Value(1);

        if (this.props.navigation.state.params) {
            const {page} = this.props.navigation.state.params;
            if (page === "unAuthorized") {
                alert('Вы не зарегистрированы')
            }
        }
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));

        this.setState({launchAnimation: 'inActive'})

        // this.onChange=_.debounce(this.onChange.bind(this), 1000);
    }

    spinValue = new Animated.Value(0);
    _animValue2 = new Animated.Value(0);

    componentWillUpdate(nextProps: Readonly<P>, nextState: Readonly<S>, nextContext: any): void {
        if(this.context !== nextContext ){
            this.state.animButtonStyle = animButtonStyle(nextContext);
            this.state.st = st(nextContext);
            this.state.Container = Container(nextContext);
        }
    }

    componentDidMount() {

        Animated.loop(
            Animated.timing(
                this.spinValue,
                {
                    toValue: 1,
                    duration: 3000,
                    easing: Easing.linear,
                    useNativeDriver: true
                }
            )
        ).start();
    }

    state = {
        toScroll: false,
        keyboardHeight: 0,
        normalHeight: 0,
        shortHeight: 0,
        heightOfElementWhenKeyboardOn: '100%',
        item: 1,
        // launchAnimation: 'active',
        facebookName: '',
        isntaToken: '',
        disAnimate: true,
        menu_expanded: false
    };


    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow(e) {
        //     this.setState({toScroll: true});
        this.setState({shortHeight: Dimensions.get('window').height - e.endCoordinates.height});
        this.setState({normalHeight: Dimensions.get('window').height});
        this.setState({keyboardHeight: e.endCoordinates.height});
        this.setState({heightOfElementWhenKeyboardOn: this.state.shortHeight});
    }

    _keyboardDidHide() {
        this.setState({toScroll: false});
        this.setState({heightOfElementWhenKeyboardOn: this.state.normalHeight});
        //
    }

    handlePressIn(button) {
        if(button===1) {
            Animated.spring(this.animatedValue1, {
                toValue: 0.5
            }).start();
        }
        if(button===11) {
            Animated.spring(this.animatedValue11, {
                toValue: 0.5
            }).start();
        }

        if(button===2) {
            Animated.spring(this.animatedValue2, {
                toValue: 0.5
            }).start()
        }
        if(button===3) {
            Animated.spring(this.animatedValue3, {
                toValue: 0.5
            }).start();
        }
    }

    handlePressOut(button) {
        if(button === 1) {
            Animated.spring(this.animatedValue1, {
                duration: 2000,
                toValue: 1,
                friction: 4,
                tension: 40
            }).start()
        }
        if(button === 11) {
            Animated.spring(this.animatedValue11, {
                duration: 2000,
                toValue: 1,
                friction: 4,
                tension: 40
            }).start()
        }
        if(button === 2) {
            Animated.spring(this.animatedValue2, {
                duration: 2000,
                toValue: 1,
                friction: 4,
                tension: 40
            }).start()
        }
        if(button === 3) {
            Animated.spring(this.animatedValue3, {
                duration: 2000,
                toValue: 1,
                friction: 4,
                tension: 40
            }).start();
        }
    }

    readText() {
        // this.props.userAction.readText()
    }

    redirectToCamera() {
        let {navigate} = this.props.navigation;
        navigate('textCamera');
    }
    redirectToBarcodeCamera() {
        let {navigate} = this.props.navigation;
        navigate('BarCodeScanner');
    }
    enterTheNumber() {
         let {navigate} = this.props.navigation;
         navigate('InputCodePage');
    }
    redirectToList(){
        let {navigate} = this.props.navigation;
        navigate('ItemInfoFromBase');
    }

    async changeTheme(color){
        return this.props.changeTheme(color)
    }



    render() {
        // const { t, i18next } = Trans;
        const { animButtonStyle, st, Container } = this.state;
        const { t } = this.props;
        const {toScroll} = this.state;
        const inputAccessoryViewID = "uniqueID";

        const spin = this.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        })

        const slide2 = this._animValue2.interpolate({
            inputRange: [0, 1],
            outputRange: [-500, 0],
        })

        const animatedStyle1 = {transform: [{scale: this.animatedValue1}]};
        const animatedStyle11 = {transform: [{scale: this.animatedValue11}]};
        const animatedStyle2 = {transform: [{scale: this.animatedValue2}]};
        const animatedStyle3 = {transform: [{scale: this.animatedValue3}]};
        return (
            <TouchableWithoutFeedback style={ Container.WholeContainerTWFeedback }
                                      // onPressIn={ ()=>{alert('al;sdkupobijds;fkg')}}
                                      // onPressIn={() => this.List()}
            >
            <View style={ Container.MainContainer }>
                <View style={ Container.container }>
                    <View style={ Container.upContainer }>
                        {/*<Image style={{width: 215, height: 200}} source={require('../images/SmartITLogo.png')} />*/}
                        <Animated.Image
                            style={{transform: [{rotateY: spin}], width: 215, height: 200, marginLeft: '20%'}}
                            source={require('../../images/SmartITLogo.png')}
                        />
                        {/*<TouchableWithoutFeedback onPress={this.changeTheme.bind(this,'GRAY')}>*/}
                        {/*    <View style={animButtonStyle.buttonStyle}>*/}
                        {/*        /!*<Trans i18nKey="userMessagesUnread">*!/*/}
                        {/*            <Text style={ animButtonStyle.buttonText }>*/}
                        {/*               gray { t('testButton:theme')}*/}
                                    <Text> { i18next.t('testButton:theme') } </Text>
                                    <Text> { i18next.t('hello') } </Text>
                        {/*            </Text>*/}
                        {/*        /!*</Trans>*!/*/}
                        {/*    </View>*/}
                        {/*</TouchableWithoutFeedback>*/}
                        {/*<TouchableWithoutFeedback onPress={this.changeTheme.bind(this,'DARK')}>*/}
                        {/*    <View style={animButtonStyle.buttonStyle}>*/}
                        {/*        <Text style={ animButtonStyle.buttonText }>dark Theme</Text>*/}
                        {/*    </View>*/}
                        {/*</TouchableWithoutFeedback>*/}
                    </View>
                    {/*<View style={{flex:1}}>*/}
                    {/*    <TouchableWithoutFeedback*/}
                    {/*        onPressIn={this.handlePressIn.bind(this,1)}*/}
                    {/*        onPressOut={this.handlePressOut.bind(this,1)}*/}
                    {/*                              onPress={this.redirectToCamera.bind(this)}>*/}
                    {/*        <Animated.View style={[animButtonStyle.buttonStyle, animatedStyle1,]}>*/}
                    {/*            <Text style={ animButtonStyle.buttonText }>Сканировать накладную</Text>*/}
                    {/*        </Animated.View>*/}
                    {/*    </TouchableWithoutFeedback>*/}
                    {/*</View>*/}
                    {/*<View style={{flex:1}}>*/}
                    {/*    <TouchableWithoutFeedback onPressIn={this.handlePressIn.bind(this,11)}*/}
                    {/*                              onPressOut={this.handlePressOut.bind(this,11)}*/}
                    {/*                              onPress={this.redirectToList.bind(this)}>*/}
                    {/*        <Animated.View style={[animButtonStyle.buttonStyle, animatedStyle11,]}>*/}
                    {/*            <Text style={ animButtonStyle.buttonText } >На гравировку</Text>*/}
                    {/*        </Animated.View>*/}
                    {/*    </TouchableWithoutFeedback>*/}
                    {/*</View>*/}
                    {/*<View style={{flex:1}}>*/}
                    {/*    <TouchableWithoutFeedback onPressIn={this.handlePressIn.bind(this,2)}*/}
                    {/*                              onPressOut={this.handlePressOut.bind(this,2)}*/}
                    {/*                              onPress={this.redirectToBarcodeCamera.bind(this)}*/}
                    {/*    >*/}
                    {/*        <Animated.View style={[animButtonStyle.buttonStyle, animatedStyle2,]}>*/}
                    {/*            <Text style={ animButtonStyle.buttonText }>Сканировать код</Text>*/}
                    {/*        </Animated.View>*/}
                    {/*    </TouchableWithoutFeedback>*/}
                    {/*</View>*/}
                    {/*<View style={{flex:1}}>*/}
                    {/*    <TouchableWithoutFeedback onPressIn={this.handlePressIn.bind(this,3)}*/}
                    {/*    onPressOut={this.handlePressOut.bind(this,3)}*/}
                    {/*    onPress={this.enterTheNumber.bind(this)}*/}
                    {/*    >*/}
                    {/*    <Animated.View style={[animButtonStyle.buttonStyle,animatedStyle3]}>*/}
                    {/*            <Text style={ animButtonStyle.buttonText } >Ввести код вручную</Text>*/}
                    {/*        </Animated.View>*/}
                    {/*    </TouchableWithoutFeedback>*/}
                    {/*</View>*/}
                </View>

            </View>
            </TouchableWithoutFeedback>
        );
    }
}

const mapStateToProps = (state) => {
    console.log(state.dimension);
    return {
        users: state.userReducer.allUsers,
        // gists: state.userReducer.gists
        user: state.userReducer.login,
        lng: state.common.lng,

    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        userAction: bindActionCreators(userAction, dispatch),
        changeTheme: bindActionCreators(changeTheme, dispatch),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MainMenu);
