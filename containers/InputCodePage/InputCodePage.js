import React from 'react';
import {
    StyleSheet,
    Text,
    Button,
    View,
    TextInput,
    TouchableOpacity,
    Platform,
    ScrollView,
    KeyboardAvoidingView,
    Animated,
    Keyboard,
    Dimensions,
    Image,
    Easing,
    TouchableHighlight,
    TouchableWithoutFeedback,
} from 'react-native';

import {connect} from "react-redux";
import userAction from '../../actions/userAction';
import {bindActionCreators} from 'redux';
// import {FormLabel, FormInput, FormValidationMessage} from 'react-native-elements'
import _ from 'lodash';
import BadInstagramCloneApp from '../TextCamera/textCamera';

// import {Input} from 'react-native-elements';


import {StackActions, NavigationActions} from 'react-navigation';

const IMAGE_HEIGHT = 408;
const IMAGE_HEIGHT_SMALL = 24;
import Cookie from 'react-native-cookie'


class InputCodePage extends React.Component {
    spinValue = new Animated.Value(0);
    _animValue2 = new Animated.Value(0);
    state = {
        toScroll: false,
        keyboardHeight: 0,
        normalHeight: 0,
        shortHeight: 0,
        heightOfElementWhenKeyboardOn: '100%',
        item: 1,
        // launchAnimation: 'active',
        facebookName: '',
        isntaToken: '',
        disAnimate: true,
        menu_expanded: false,
        text: '',
        isKeyboardActive: false,
        serialNumber: ''
    };

    constructor(props) {
        super(props);
        this.myRef = React.createRef();
        this.y_translate = new Animated.Value(0);

    }

    // spring = new Animated.Value(0);

    componentWillMount() {
        this.animatedValue1 = new Animated.Value(1);

        if (this.props.navigation.state.params) {
            const {page} = this.props.navigation.state.params;
            if (page === "unAuthorized") {
                alert('Вы не зарегистрированы')
            }
        }
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));

        this.setState({launchAnimation: 'inActive'})

        // this.onChange=_.debounce(this.onChange.bind(this), 1000);
    }

    componentDidMount() {

        Animated.loop(
            Animated.timing(
                this.spinValue,
                {
                    toValue: 1,
                    duration: 3000,
                    // easing: Easing.linear,
                    useNativeDriver: true
                }
            )
        ).start();
        this.setState({
            text: this.props.resParseText.text,
            companyName: this.props.resParseText.companyName,
            formNumber: this.props.resParseText.formNumber,
            IncomeOrder: this.props.resParseText.IncomeOrder
        });
        // alert('this is from editing page',this.state.text);
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow(e) {
        //     this.setState({toScroll: true});
        //     this.setState({shortHeight: Dimensions.get('window').height - e.endCoordinates.height});
        //     this.setState({normalHeight: Dimensions.get('window').height});
        //     this.setState({keyboardHeight: e.endCoordinates.height});
        //     this.setState({heightOfElementWhenKeyboardOn: this.state.shortHeight});
        this.setState({isKeyboardActive: true})
    }

    _keyboardDidHide() {
        //     this.setState({toScroll: false});
        //     this.setState({heightOfElementWhenKeyboardOn: this.state.normalHeight});
        this.setState({isKeyboardActive: false})
    }

    netsAuthorization() {
        Animated.timing(
            this._animValue2,
            {
                toValue: 1,
                duration: 2000,
                easing: Easing.bounce,
                useNativeDriver: true
            }
        ).start();
        this.setState({disAnimate: false})
    }

    authPanelDisAnimate() {
        console.log('Test press');

        Animated.timing(
            this._animValue2,
            {
                toValue: 0,
                duration: 2000,
                easing: Easing.bounce,
                useNativeDriver: true
            }
        ).start();
        this.setState({disAnimate: true})
    }

    handlePressIn() {
        Animated.spring(this.animatedValue1, {
            toValue: 0.5
        }).start()
    }

    handlePressOut() {
        Animated.spring(this.animatedValue1, {
            duration: 2000,
            toValue: 1,
            friction: 4,
            tension: 40
        }).start()
    }

    readText() {
        // this.props.userAction.readText() //because of it has no arguments passed through.
    }

    redirectToCamera() {
        let {navigate} = this.props.navigation;

        navigate('BadInstagramCloneApp');
    }

    confirmSerialNumber() {
        this.props.userAction.getCodeResponse(this.state.serialNumber)
            .then(()=> {
                this.props.navigation.navigate('InfoFromBaseByNumber');
            });
    }

    render() {
        const {toScroll} = this.state;
        const inputAccessoryViewID = "uniqueID";

        const spin = this.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        })

        const slide2 = this._animValue2.interpolate({
            inputRange: [0, 1],
            outputRange: [-500, 0],

        })
        const animatedStyle = {transform: [{scale: this.animatedValue1}]}
        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'gray',
                height: "100%"
            }}>
                <View style={{flex:1}}>

                </View>
                <View style={{flex:2,width:'100%',justifyContent:'center',alignItems:'center',flexDirection: 'column' }}>
                    <View style={{width:"90%"}}>
                        <View style={styles.ContainerLabel}>
                            <Text style={styles.textLabel}> Введите Номер Документа</Text>
                        </View>

                    </View>
                    <View style={{width:'90%'}}>
                        <TextInput
                            //multiline={true}
                            //numberOfLines={4}
                            //blurOnSubmit={false}
                            onChangeText={(value) => this.setState({serialNumber:value})}
                            //value={this.props.resParseText.text}
                            //value={this.state.text}
                            style={styles.textInput}
                        />
                    </View>
                    {/*<ScrollView*/}
                    {/*style={{width:'90%'}}*/}
                    {/*// style={mainContainer(this.state.heightOfElementWhenKeyboardOn).container}*/}

                    {/*>*/}

                    {/*</ScrollView>*/}
                </View>

                {this.state.isKeyboardActive !== true && <View style={{flex:2}}>
                    <TouchableOpacity onPress={this.confirmSerialNumber.bind(this) }
                        // onPress={this.state.disAnimate ? this.netsAuthorization.bind(this) : this.authPanelDisAnimate.bind(this)}
                                      style={{
                                          borderWidth: 1,
                                          // borderColor:'rgba(0,0,0,0.2)',
                                          borderColor: 'black',
                                          alignItems: 'center',
                                          justifyContent: 'center',
                                          width: 200,
                                          height: 50,
                                          backgroundColor: 'gray',
                                          borderRadius: 50,
                                      }}
                    ><Text>Подтвердить</Text>
                    </TouchableOpacity>
                </View>}

                {/*</KeyboardAvoidingView>*/}

            </View>


        );
    }
}

const styles = StyleSheet.create({
    ContainerLabel: {
        marginTop: 10,
        // height: 50,
        width: '100%',
        // backgroundColor: '#eee',
        // borderWidth:1,
        // borderRadius:25,
        justifyContent: 'center',
    },
    textLabel: {
        textAlign: 'center',
        alignItems: 'center',
    },
    textInput: {
        borderColor: 'black',
        marginTop: 10,
        height: 50,
        width: '100%',
        backgroundColor: '#eee',
        borderWidth: 1,
        borderRadius: 25,
        justifyContent: 'center',
        textAlign: "center"
    }
});

const animButtonStyle = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonStyle: {

        height: 50,
        // width:100,
        borderRadius: 25,
        backgroundColor: 'black',

    }
})

const st = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    label: {
        fontSize: 16,
        fontWeight: 'normal',
        marginBottom: 48,
    },
});

const Container = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        // justifyContent: 'space-around'
        justifyContent: 'center'
    },
});

const mapStateToProps = (state) => {
    console.log(state.dimension);
    return {
        resParseText: state.userReducer.resParseText
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        userAction: bindActionCreators(userAction, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(InputCodePage);
