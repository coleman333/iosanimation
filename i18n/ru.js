export default {
    testButton: {
        theme: 'серая тема'
    },
    Settings:{
        portNumber: 'Введите номер порта',
        addressConnection: 'Введите адрес соединения',
        themeDark: 'ТЕМНАЯ',
        themeGray: 'СЕРАЯ',
        themeEn: 'Англ',
        themeRu: 'Рус',
        confirmButton: 'Подтвердить'
    }

}
