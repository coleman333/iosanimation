// export const resources = {
//   en: {
//     translation: {
//       "Welcome to React": "Welcome to React and react-i18next !!!!"
//     },
//   },
//   ru: {
//     translation: {
//       "Welcome to React": "привет мир"
//     },
//   }
// };

export default {
    testButton: {
        theme: 'grey theme'
    },
    Settings:{
        portNumber: 'Enter port number',
        addressConnection: 'Enter address of connection ',
        themeDark: 'DARK',
        themeGray: 'GRAY',
        themeEn: 'Ang',
        themeRu: 'Rus',
        confirmButton: 'Confirm'
    },
    common: {
        'off all purchases': 'off all purchases',
        'valid until': 'Valid until',
        ratings: 'ratings',
        'input text': 'Input text',
        buttons: {
            verify: 'Verify',
            Login: 'Sign Up / Login',
            Scan: 'Scan or Tap',
            'get more': 'Get more',
            'How it works': 'How it works',
        },
        error: {
            title: 'Oops!\n Something went\n wrong here',
            message: 'We are dedicated to making\n our product better every day.\nHelp us do this by sending a\n photo of the bottle to:',
            email: 'support@omniaz.io',
        },
    },
    Login: {
        title: 'Log in',
        loginWith: 'Log in with',
        loginWithFB: 'Facebook',
        loginWithGoogle: 'Google',
    },
    WalletTab: {
        'Get more credits': 'Get more credits and enjoy more rewards.',
    },
    WelcomeScreen: {
        hey: 'Hey there!',
    },
    HowItWorks: {
        next: 'Next',
        step1: 'Step 1',
        slide1:
            'Discover the very best of\n local and international craft\n beers, wines, and spirits,\n around you.',
        step2: 'Step 2',
        slide2:
            'Tap or scan to learn where\n the bottle came from - who\n made it, where and when it\n was made.',
        step3: 'Step 3',
        slide3: 'Find similar bottle, food\n pairings, nearest retailers\n and pricing.',
        step4: 'Step 4',
        slide4: 'Sign up now to reap\n fantastic rewards and\n money-of coupons for using\n the App.',
    },
    Recommended: {
        title: 'You may also like...',
    },
    Rewards: {
        title: 'Rewards',
    },
    Welcome: {
        title: 'Welcome',
    },
    Retailers: {
        title: 'Retailers near you',
    },
    FacebookReward: {
        title: 'Like us on Facebook:',
        'follow us': 'Follow our Facebook Page',
        'like & share': 'Like and Share our post',
        'enter URL': 'Enter the URL to your Facebook page',
    },
    InstagramReward: {
        title: 'Follow us on Instagram',
        'follow us': 'Follow our Instagram account',
        'like & share': 'Like and Share our post',
        'enter URL': 'Enter the URL to your Instagram account',
    },
    TwitterReward: {
        title: 'Follow us on Twitter',
        'follow us': 'Follow our Twitter page',
        'like & share': 'Like and Share our post',
        'enter URL': 'Enter the URL to your Twitter page',
    },
    Bounties: {
        title: 'Bounties',
    },
    Coupons: {
        title: 'Coupons',
        MyCoupons: 'My Coupons',
        noCoupons: 'You don\'t have any coupons? Don\'t worry, we\'ll send you some soon.',
    },
    Coupon: {
        expires: 'EXPIRES IN:',
        day: 'day',
        days: 'days',
        couponCode: 'COUPON CODE:',
        redeem: 'REDEEM NOW',
        signup: 'SIGN UP NOW',
    },
    Profile: {
        noscans: 'No scans of this category',
        viewmore: 'VIEW MORE',
        monthNames: {
            '0': 'January',
            '1': 'February',
            '2': 'March',
            '3': 'April',
            '4': 'May',
            '5': 'June',
            '6': 'July',
            '7': 'August',
            '8': 'September',
            '9': 'October',
            '10': 'November',
            '11': 'December'
        },
        tabs: {
            'wine': 'Wine',
            'beer': 'Beer',
            'spirits': 'Spirits',
        },
        form: {
            'first_name': 'First Name',
            'last_name': 'Last Name',
            'email': 'Email',
            'birth_date': 'Date of birth',
            'country': 'Country',
            'headerTitle': 'PROFILE SETTINGS'
        },
        buttons: {
            'update': 'Update',
            'logout': 'Logout',
            'delete': 'Delete Account',
            'save': 'save'
        },
        'Terms': 'Our Terms of Service ',
        'conjunction': ' and ',
        'Policy': ' Privacy Policy',
        'mystery_Bottle': 'Mystery \n Bottle',
        deleteModal: {
            'title': 'Are you sure?',
            'text': 'Deleting your account \n removes all your data, \n Credits, and Coupons.' +
                ' \n\n You cannot recover your \n account after deleting it.',
            'deleteButtonText': 'DELETE MY ACCOUNT',
            'keepButtonText': 'KEEP MY ACCOUNT',
        }
    },
    ScanTypes: {
        nfc: 'NFC Scans',
        qr: 'QR Scans',
        credits: 'Credits',
    },
    Scan: {
        qr: 'QR',
        nfc: 'NFC',
        label: 'Label',
        nfc_message: 'Enable NFC on your phone then\n tap against Smart Tag',
        camera_permission_title: 'Permission to use camera',
        camera_permission_text: 'We need your permission to use your camera phone',
        retake: 'Retake',
        upload: 'Upload',
        preview_message: 'Use this picture?',
        scan_method: 'Please select scan method below',
        error: {
            titleLabel: 'Let’s try this again…',
            messageLabel: 'We weren’t able to find a match for that bottle.\n \n Let’s try taking the photo again.',
            buttonLabel: 'Sure!',
        },
        closest: {
            title: 'Here’s what we found…',
            message: 'We weren’t able to find a perfect match, but we think it’s one of these.',
        },
        photo_overlay_text: 'Position the bottle label within frame',
    },
    Product: {
        price_label: 'Average price',
        buy_text: 'Buy at wine connection',
        omniaz_id: 'Omniaz ID',
        description: 'Description',
        awards: 'Awards',
        aromas: 'Aromas',
        'food pairings': 'Food pairings',
        'tasting notes': 'Tasting notes',
        traceability: 'Traceability',
        producer: 'Producer',
        retailer: 'Retailer',
        reviews: 'Reviews',
        'retailers near you': 'Retailers near you',
        'products you may like': 'Products you may like',
        banner: {
            earned: 'You have just earned\n 360 OMZ credits.',
            signup: 'Sign up and get\n your credits',
        },
        stars_button_text: {
            buttonLabel: 'RATE IT',
            modalText: 'let other DRNKERS know \n what you think'
        },
        mysteryBottle: {
            'title': 'Description',
            'text': 'This bottle is a bit of a mystery to us. Help \n us identify it by sending a photo of it to \n support@omniaz.io.' +
                'Don`t forget to sign up \n and get your coupon!'
        }
    },
    ProfileSettings: {
        title: 'Profile Settings',
    },
}