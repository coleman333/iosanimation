import { createAction } from "redux-actions";

module.exports = {
    CHANGE_DIMENSIONS: createAction('CHANGE_DIMENSIONS')
};