import { createAction } from 'redux-act';
import {AsyncStorage} from "react-native";

module.exports.default = module.exports =  {
    addProcess: createAction('ADD_PROCESS'),
    removeProcess: createAction('REMOVE_PROCESS'),
    changeTheme: createAction('CHANGE_THEME', async (color)=>{
        await AsyncStorage.setItem('THEME', color);
        return color;
    }),
    changeLang: createAction('CHANGE_LANG', async (lng)=>{
        await AsyncStorage.setItem('lng', lng);
        return lng;
    }),
};
